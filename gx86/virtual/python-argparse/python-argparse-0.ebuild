# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-python/argparse/argparse-1.2.1.ebuild,v 1.15 2012/08/05 14:08:38 ryao Exp $

EAPI=4
PYTHON_COMPAT=(
	python2_5 python2_6 python2_7
	python3_1 python3_2
	jython2_5
)
inherit python-r1

DESCRIPTION="A virtual for the Python argparse module"
HOMEPAGE="http://code.google.com/p/argparse/ http://pypi.python.org/pypi/argparse"
SRC_URI=""

LICENSE=""
SLOT="0"
KEYWORDS="alpha amd64 arm hppa ia64 m68k ~mips ppc ppc64 s390 sh sparc
	x86 ~ppc-aix ~amd64-fbsd ~x86-fbsd ~x64-freebsd ~x86-freebsd
	~x86-interix ~amd64-linux ~x86-linux ~ppc-macos ~x64-macos
	~x86-macos ~m68k-mint ~sparc-solaris ~sparc64-solaris ~x86-solaris"
IUSE=""

setup_globals() {
	local i

	RDEPEND=${PYTHON_DEPS}
	for i in "${PYTHON_COMPAT[@]}"; do
		case "${i}" in
			python2_7|python3*)
				;;
			*)
				# Note: add USE-deps when dev-python/argparse starts
				# supporting PYTHON_TARGETS
				RDEPEND+=" python_targets_${i}? ( dev-python/argparse )"
				;;
		esac
	done
}
setup_globals
